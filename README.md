# Optimization

## Maximum and Minimum Definitions

Let $`f`$ be a function of one variable for all $`x`$ in some domain $`D`$.

* A **global** *maximum* of $`f`$ is a point $`x_0`$ in $`D`$ such that $`f(x) \le f(x_0)`$ for all $`x`$ in $`D`$.

For a constant $`\Delta > 0`$, the neighborhood $`N_\Delta (x_0)`$ of a point $`x_0`$ is the set of all points $`x`$ such that
$`x_0 - \Delta < x < x_0 + \Delta`$.

* A **local** *maximum* of $`f`$ is a point $`x_0`$ if there esists $`\Delta > 0`$ such that $`f(x) \le f(x_0)`$ for all $`x`$ in
$`N_\Delta (x_0)`$ where $`f(x)`$ is defined.

Local and global *minimum* are similarly defined.

## Finding Extrema

Extrema whether local or global, can occur in three places:

1. At a boundary of the domain.

2. At a point without a derivative.

3. At a point $`x_0`$ with $`f^{\prime} (x_0) = 0`$.

If $`f\prime (x_0) = 0`$ three new possibilities may arise: $`x_0`$ is a *maximum*, a *minimum* or *neither*.

Let $`f`$ be twice differentiable in a neighborhood of $`x_0`$:

- If $`f^{\prime} (x_0) = 0`$ and $`f^{\prime \prime} (x_0) > 0`$, then $`x_0`$ is a minimum.

- If $`f^{\prime} (x_0) = 0`$ and $`f^{\prime \prime} (x_0) < 0`$, then $`x_0`$ is a maximum.

- If $`f^{\prime} (x_0) = 0`$ and $`f^{\prime \prime} (x_0) = 0`$, then $`x_0`$ may not be an extremum.

## Binary Search

Is a very simple idea for solving numerically $`f(x) = 0`$. It requires:

1. The function $`f`$ to be continuous.

2. A pair of values $`(a, b)`$ such that $`f(a) > 0`$ and $`f(b) < 0`$.

Then a desired value $`x_0`$ such that $`f(x_0) = 0`$ can be found between $`a`$ and $`b`$.

The method consists in computing $`f(c)`$ for $`c = \frac{a+b}{2}`$.

- If $`f(c) > 0`$, the procedure is repeated with the pair $`(c, b)`$.

- If $`f(c) < 0`$, the procedure is repeated with the pair $`(a, c)`$.

- If $`f(c) = 0`$, the procedure terminates.

This method converges rapidly, getting a good precision within $`20`$ iterations.

## Golden Section Search

Similar to binary search. Used to compute the maximum of a function $`f(x)`$ defined on an interval
$`[a, b]`$, when derivative method is not applicable. It assumes:

1. $`f`$ is continuous.

2. $`f`$ has a unique local maximum in the interval $`[a, b]`$.

It consists of computing $`f(c)`$ and $`f(d)`$ for $`a < d < c < b`$.

* If $`f(c) > f(d)`$, the procedure is repeated in the interval $`[d, b]`$.

* If $`f(c) < f(d)`$, the procedure is repeated in the interval $`[a, c]`$.

The name comes from the choice of selecting $`c`$ and $`d`$ as:

```math
c = a + r(b-a)
d = b + r(a-b)
```

where $`r`$ is chosen to be the *golden ratio*:

```math
r = \frac{\sqrt{5} - 1}{2} = 0.618034...
```

which yields fast convergance.

## Convexity

Checking that an extrema is global is very hard except for convex and concave functions.

- A convex function is one where the line segment connection two points $`(x, f(x))`$ and $`(y, f(y))`$
lies above the function.

- Mathematically, $`f`$ is convex if for all $`x, y`$ and all $`0 < \alpha < 1`$:

```math
f \left( \alpha x + (1-\alpha)y \right) \le \alpha f(x) + (1 - \alpha) f(y)
```

The function $`f`$ is concave if $`-f`$ is convex.

- If $`f`$ is twice differentiable: the function $`f`$ is convex on some domain $`[a, b]`$ 
iff $`f^{\prime \prime}(x) \ge 0`$ for all $`x`$ in the domain.

- Similarly $`f`$ is concave on some domain $`[a, b]`$ iff $`f^{\prime \prime}(x) \le 0`$
for all $`x`$ in the domain.

- If $`f(x)`$ is convex, then any local minimum is also a global minimum.

- If $`f(x)`$ is concave, then any local maximum is also a global maximum.

## Gradient

- Given a function $`f`$ of $`n`$ variables $`(x_1, x_2, ..., x_n)`$, we define the partial
derivative *relative* to variable $`x_i`$, written as $`\frac{\delta f}{\delta x_i}`$ to be the
derivative of $`f`$ with respect to $`x_i`$, treating all variables except $`x_i`$ as a constant.

- Let $`x`$ denote the vector $`(x_1, x_2, ..., x_n)`$. The gradient of $`f`$ at $`x`$ (written $`\Delta f(x)`$)
is the vector:

```math
\Delta f(x) = \begin{bmatrix}
\frac{\delta f(x)}{\delta x_1} \\
\frac{\delta f(x)}{\delta x_2} \\
\vdots \\
\frac{\delta f(x)}{\delta x_n}
\end{bmatrix}
```

- $`\Delta f(x)`$ gives the direction of the *steepest ascent* of the function $`f`$ at point $`x`$.

- The gradient act as the derivative in that small changes around a given point $`x^{*}`$ can be estimated using it:

```math
f(x^{*} + \Delta) \approx f(x^{*}) + \Delta \nabla f(x^{*})
```

## Hessian

Second partials $`\frac{\delta^{2} f}{\delta x_i \delta x_j}`$ are obtained from $`f(x)`$ by taking the
derivative relative to $`x_i`$ (yielding $`\frac{\delta f(x)}{\delta x_i}`$) and then the derivative
relative to $`x_j`$. So we can compute $`\frac{\delta^2 f(x)}{\delta x_1 \delta x_1}`$, 
$`\frac{\delta^2 f(x)}{\delta x_1 \delta x_2}`$, and so on, arranged in the Hessian matrix:

```math
H(x) = \begin{bmatrix}
\frac{\delta^2 f(x)}{\delta x_1 \delta x_1} & \frac{\delta^2 f(x)}{\delta x_1 \delta x_2} & \cdots & \frac{\delta^2 f(x)}{\delta x_1 \delta x_n} \\
\frac{\delta^2 f(x)}{\delta x_2 \delta x_1} & \frac{\delta^2 f(x)}{\delta x_2 \delta x_2} & \cdots & \frac{\delta^2 f(x)}{\delta x_2 \delta x_n} \\
\vdots & \vdots & \ddots & \vdots \\
\frac{\delta^2 f(x)}{\delta x_n \delta x_1} & \frac{\delta^2 f(x)}{\delta x_n \delta x_2} & \cdots & \frac{\delta^2 f(x)}{\delta x_n \delta x_n} \\
\end{bmatrix}
```

The Hessian is a symmetric matrix. That is $`\frac{\delta^2 f}{\delta x_i \delta x_j} = \frac{\delta^2 f}{\delta x_j \delta x_i}`$.

Note that the Hessian of a function is the **jacobian** of its *Gradient*.

## Multivariate Maximum and Minimum Definitions

For a function $`f`$ of $`n`$ variables, optima can occur in three places:

1. At a boundary of the domain.

2. At a point without a derivative.

3. At a point $`x^{*}`$ with $`\nabla f(x^{*}) = 0`$.

If $`\nabla f(x^{*}) = 0`$ three new possibilities may arise:

- If $`H(x^{*})`$ is positive definite, then $`x^{*}`$ is a local minimum.

- If $`H(x^{*})`$ is negative definite, then $`x^{*}`$ is a local maximum.

These properties can be checked by computing the determinats of the principal minors.

## Global Optima

We say that a domain is convex if every line drawn between two points in the dmoain lies within the domain.

We say a function $`F`$ is convex is the line connectiong any two points lies above the function. That is, for all $`x, y`$
in the domain and $`0 < \alpha < 1`$ we have:

```math
f\left( \alpha x + (1-\alpha)y \right) \le \alpha f(x) + (1-\alpha) f(y)
```

* If a function is convex on a convex domain then any local minimum is a global minimum.

* If a function is concave on a convex domain then any local maximum is a global maximum.

To check that a function is convex on a domain check that its Hessian matrix $`H(x)`$ is
positive semidefinite for every point $`x`$ in the domain.

To check that the function is concave, check that its Hessian is negativa semidefinite
for every point $`x`$ in the domain.

## Constrained Optimization (Equality Constraints)

The problem is to minimize (or maximize) $`f(x)`$ subject to:

```math
\begin{matrix}
g_1 (x) = b_1 \\
g_2 (x) = b_2 \\
\vdots \\
g_m (x) = b_m
\end{matrix}
```

A solution can be found using the **Lagrangian**:

```math
L(x, \lambda ) = f(x) + \sum_{i=1}^m \lambda _i(b_i - g_i (x))
```

Which can be rewritten as:

```math
L(x, \lambda ) = f(x) - \sum_{i=1}^m \lambda _i(g_i (x) - b_i)
```

Each $`\lambda _i`$ gives the cost associated with the constraint $`i`$.

* Assume $`x^{*} = (x_1 ^{*}, x_2 ^{*}, \cdots, x_n ^{*})`$ maximizes or minimizes $`f(x)`$ subject to the constraints $`g_i (x) = b_i`$ for $`i = 1, 2, ..., m`$. Then either:

1. The vectors $`\nabla g_1 (x^{*}), \nabla g_2 (x^{*}), ..., \nabla g_m (x^{*})`$ are linearly dependent or,

2. There exists a vector $`\lambda ^{*} = (\lambda _1 ^{*}, \lambda _2 ^{*}, ..., \lambda _m ^{*})`$ such that $`\nabla L(x^{*}, \lambda ^{*}) = 0`$, i.e:

```math
\frac{\delta L}{\delta x_1}(x^{*}, \lambda ^{*}) = \frac{\delta L}{\delta x_2}(x^{*}, \lambda ^{*}) = \cdots = \frac{\delta L}{\delta x_n}(x^{*}, \lambda ^{*}) = 0
```

and

```math
\frac{\delta L}{\delta \lambda _1}(x^{*}, \lambda ^{*}) = \frac{\delta L}{\delta \lambda _2}(x^{*}, \lambda ^{*}) = \cdots = \frac{\delta L}{\delta \lambda _m}(x^{*}, \lambda ^{*}) = 0
```

* Case 1 cannot occur when here is only one constraint.

* When solving optimization problems with equality constraints, we only look at the solutions $`x^{*}`$ that satisfy Case 2.

* Note that the equation $`\frac{\delta L}{\delta \lambda _i}(x^{*}, \lambda ^{*}) = 0`$ is nothing more than $`b_i - g_i (x^{*}) = 0`$, in other words, taking the partial derivative with respect to $`\lambda`$ does nothing more than return the original constraints.

* If $`f(x)`$ is concave and all of the $`g_i (x)`$ are *linear*, then any feasible $`x^{*}`$ with a corresponding $`\lambda ^{*}`$ making $`\nabla L(x^{*}, \lambda ^{*}) = 0`$ maximizes $`f(x)`$ subject to the constraints. Similarly, if $`f(x)`$ is convex, it minimizes it.

### Example

Minimize $`2x_1 ^{2} + x_2 ^{2}`$ subject to $`x_1 + x_2 = 1`$. The Lagrangian then is:

```math
L(x_1 , x_2 , \lambda ) = 2x_1 ^{2} + x_2 ^{2} + \lambda (1 - x_1 - x_2)
```

Taking the partial derivatives:

$`\frac{\delta L}{\delta x_1} = 4x_1 ^{*} - \lambda ^{*} = 0`$

$`\frac{\delta L}{\delta x_2} = 2x_2 ^{*} - \lambda ^{*} = 0`$

$`\frac{\delta L}{\delta \lambda} = 1 - x_1 ^{*} - x_2 ^{*} = 0`$

* The first two equations imply $`2x_1 ^{*} = x_2 ^{*}`$. Substituting into the third equation gives $`x_1 ^{*} = 1/3`$, $`x_2 ^{*} = 2/3`$ and $`\lambda ^{*} = 4/3`$ with a function value of $`2/3`$.

* Since $`f(x_1 , x_2 )`$ is convex (its Hessian 
$`H(x_1 , x_2 ) = \begin{bmatrix} 
4 & 0 \\
0 & 2
\end{bmatrix}`$ is positive definite) and $`g(x_1 , x_2 )`$ is a linear function, the above solution minimizes $`f(x_1 , x_2 )`$ subject to the constraints.

* The gradient of $`f`$ and $`g`$ at the optimum point must point in the same direction, though they may have different lengths. This implies $`\nabla f(x^{*}) = \lambda ^{*}\nabla g(x^{*})`$.

## Constrained Optimization (Inequality Constraints)

The problem is to **maximize** $`f(x)`$ subject to:

```math
\begin{matrix}
g_1 (x) = b_1 \\
g_2 (x) = b_2 \\
\vdots \\
g_m (x) = b_m \\
h_1 (x) \le d_1 \\
h_2 (x) \le d_2 \\
\vdots \\
h_p (x) \le d_p
\end{matrix}
```

Constraints with $`\ge`$ can be converted to $`\le`$ constraints by multiplying by $`-1`$ and converting maximization to minimization. If minimization is paired with $`\ge`$ constraints, then to change is needed.

* The Lagrangian is:

```math
L(x, \lambda , \mu ) = f(x) + \sum _{i=1}^m \lambda _i (b_i - g_i (x)) + \sum _{j=1}^p \mu _j (d_j - h_j (x))
```

* Assume $`x^{*} = (x_1 ^{*}, x_2 ^{*}, \cdots, x_n ^{*})`$ **maximizes** $`f(x)`$ subject to the constraints $`g_i (x) = b_i`$ for $`i = 1, 2, ..., m`$ and $`h_j (x) \le d_i`$ for $`j = 1, 2, ..., p`$. Then either:

1. The vectors $`\nabla g_1 (x^{*}), \nabla g_2 (x^{*}), ..., \nabla g_m (x^{*}), \nabla h_1 (x^{*}), \nabla h_2 (x^{*}), ..., \nabla h_p (x^{*})`$ are linearly dependent or,

2. There exists a vector $`\lambda ^{*} = (\lambda _1 ^{*}, \lambda _2 ^{*}, ..., \lambda _m ^{*})`$ and $`\mu ^{*} = (\mu _1 ^{*}, \mu _2 ^{*}, ..., \mu _p ^{*})`$ such that:

```math
\nabla f(x^{*}) - \sum _{i=1}^m \lambda _i ^{*} \nabla g_i (x^{*}) - \sum _{j=1}^p \mu _{j} ^{*} \nabla h_j (x^{*}) = 0
```

And complementary condition:

```math
\mu _j ^{*}(h_j (x^{*}) - d_j ) = 0
```

with:

```math
\mu _j ^{*} \ge 0
```

* In general, to solve these equations, you begin with complementary and note that either $`\mu _j ^{*}`$ must be zero or $`h_j ^{*} - d_j = 0`$. Based on the various posibilities, you come up with one or more candidate solutions. If there is an optimal solution, one of the candidates will be it.

* The above conditions are called the *Kuhn-Tucker* conditions. For $`x^{*}`$ optimal, some inequalities will be tight and some not. Those not tight can be ignored (corresponding $`\mu _j ^{*} = 0`$). Those that are tight can be treated as **equalities** which lends to the simple Lagrangian approach. So the *complementary* conditions forces either $`\mu _j ^{*} = 0`$ or the constraint to be tight (equality).

### Example

Minimize $`(x-2)^2 + 2(y-1)^2`$ subject to $`x+4y \le 3`$ and $`x \ge y`$.

* First convert to standard form. I.e. **maximize** $`-(x-2)^2 - 2(y-1)^2`$ subject to $`x+4y \le 3`$ and $`-y+x \le 0`$.

* The Lagangian is:

```math
L(x, y, \mu _1 , \mu _2 ) = -(x-2)^2 - 2(y-1)^2 - \mu _1 (3 -x -4y) + \mu _2 (0 -x + y)
```

* The optimality conditions:

$`\frac{\delta L}{\delta x} = -2(x-2) -\mu _1 + \mu _2 = 0`$

$`\frac{\delta L}{\delta y} = -4(y-1) -4\mu _1 -\mu _2 = 0`$

* The *complementary* conditions:

$`\mu _1 (3 -x -4y) = 0`$

$`\mu _2 (x-y) = 0`$

with $`\mu _1 , \mu _2 \ge 0`$

* There are two complementary conditions, so four cases to check:

1. $`\mu _1 = 0`$, $`\mu _2 = 0`$: gives $`x=2`$, $`y=1`$ which is not feasible.

2. $`\mu _1 = 0`$, $`x-y = 0`$: gives $`x=4/3`$, $`y=4/3`$ and $`\mu _2 = -4/3`$ which is not feasible.

3. $`\mu _2 = 0`$, $`3-x-4y=0`$: gives $`x=5/3`$, $`y=1/3`$, $`\mu _1 = 2/3`$ which is ok.

4. $`3-x-4y=0`$, $`x-y=0`$: gives $`x=3/5`$, $`y=3/5`$, $`\mu _1 = 22/25`$, $`\mu _2 = -48/25`$ which is not feasible.

* The solution then is $`x=5/3`$, $`y=1/3`$.

## Optimality Conditions Summary

### Single Variable (Unconstrained)

Solve $`f^{\prime} (x) = 0`$ to get candidate $`x^{*}`$.

* If $`f^{\prime \prime} (x^{*}) \ge 0`$ then $`x^{*}`$ is a local minimum.

* Else $`f^{\prime \prime} (x^{*}) \le 0`$ then $`x^{*}`$ is a local maximum.

* If $`f(x)`$ is convex then a local minimum is a global minimum.

* If $`f(x)`$ is concave then a local maximum is a global maximum.

### Multiple Variable (Unconstrained)

Solve $`\nabla f(x) = 0`$ to get candidate $`x^{*}`$.

* If $`H(x^{*})`$ is positive definite then $`x^{*}`$ is a local minimum.

* If $`H(x^{*})`$ is negative definite then $`x^{*}`$ is a local maximum.

* If $`f(x)`$ is convex then a local minimum is a global minimum.

* If $`f(x)`$ is concave then a local maximum is a global maximum.

### Multiple Variable (Equality Constrained)

Form Lagrangian $`L(x, \lambda) = f(x) - \sum _{i} \lambda _i (g_i (x) - b_i )`$.

Solve $`\nabla L(x, \lambda) = 0`$ to get candidates $`x^{*}`$ and $`\lambda ^{*}`$.

Best $`x^{*}`$ is optimum if exists.

### Multiple Variable (Equality/Inequality Constrained)

Put into standard form (maximize and $`\le`$ constraints).

Form Lagrangian $`L(x, \lambda) = f(x) - \sum _{i} \lambda _i (g_i (x) - b_i ) - \sum _{j} \mu _j (h_j (x) - d_j )`$.

Solve $`\nabla L(x, \lambda) = 0`$ (Note : no derivatives wrt $`\mu`$). 

Use complementary conditions:

```math
\mu _j ^{*}(h_j (x^{*}) - d_j ) = 0
```

($`mu _j = 0`$ or $`h_j (x^{*}) - d_j =0`$), along with:

```math
\mu _j ^{*} \ge 0
```

to get feasible candidates $`x^{*}`$, $`\lambda ^{*}`$ and $`\mu ^{*}`$.

Best $`x^{*}`$ is optimum if exists.

## Discrete LQR Solution as Optomization Problem

Minimize:

```math
J = \frac{1}{2} \sum _{t=0}^{N-1} \left( x_t ^{T} Q x_t + u_t ^{T} R u_t \right) + \frac{1}{2} x_N ^{T} Q x_N
```

subject to

```math
x_{t+1} = A x_t + B u_t
```

for $`t = 0, ..., N-1`$ where $`R = R^T`$ and $`Q = Q^T`$.

* Introduce Lagrange multipliers $`\lambda _1, \lambda _2, ..., \lambda _{N}`$ and form the Lagrangian:

```math
L = \frac{1}{2} \sum _{t=0}^{N-1} (x_t ^{T} Q x_t + u_t ^{T} R u_t ) + \frac{1}{2} x_N ^{T} Q_f x_N + \sum_{t=0}^{N-1} \lambda _{t+1}^T (Ax_t + Bu_t - x_{t+1}) 
```

### Optimality Conditions

**Note**: $`\frac{\delta x^t Ax}{\delta x} = (A + A^T )x`$ and $`\frac{\delta a^T x}{\delta x} = \frac{\delta x^T a}{\delta x} = a`$. All derivatives are in *denominator layout* (i.e. result is column vector).

* For $`t=0, ..., N-1`$, $`x_0 = x_{ini}`$, we have: 

$`x_{t+1} = Ax_t +Bu_t`$

* For $`t=0, 1, ..., N-1`$, we have: 

$`\nabla _{u_t} L = R u_t + B^T \lambda _{t+1} = 0`$.

* For $`t=1, 1, ..., N-1`$, we have: 

$`\nabla _{x_t} L = Qx_t + A^T \lambda _{t+1} - \lambda _t = 0`$.

**Note**: this derivative requires **two** samples because $`x_t`$ 
appears in $`\lambda _{t+1}^T (Ax_t + Bu_t - x_{t+1})`$ 
and in $`\lambda _{t}^T (Ax_{t-1} + Bu_{t-1} - x_{t})`$.

Hence $`\lambda _t = A^T \lambda _{t+1} + Qx_t`$.

* For $`t = N`$, we have:

$`\nabla _{x_N} L = Q_f x_N - \lambda _N = 0`$.

Hence $`\lambda _N = Q_f x_N`$.

* These are a set of linear equations in the variables:

$`u_0 , ..., u_{N-1}`$, $`x_1 , ..., x_{N}`$, $`\lambda _1 , ..., \lambda _{N}`$.

### Co-state Equations

Optimality conditions break into two parts:

1. $`x_{t+1} = A x_t + B u_t`$, with $`x_0 = x_{ini}`$ runs forward in time.

2. $`\lambda _t = A^T \lambda _{t+1} + Qx_t`$ with $`\lambda _N = Q_f x_N`$ runs backwards in time.

$`\lambda`$ is called the **co-state** and condition 2 is called the *adjoint system*.

### Ricatti Equation

The *Optimality Conditions* and the *Co-state Equations* can be used to obtain a solution in the form of a *Ricatti Equation*:

We have that $`\lambda _N = P_N x_N`$ where $`P_N = Q_f`$.

Substituting the system we get:

$`\lambda _N = P_N (Ax_{N-1}+Bu_{N-1})`$

Using the condition : $`R u_{N-1} + B^T \lambda _{N} = 0`$ we get:

$`\lambda _N = P_N ( Ax_{N-1}-BR^{-1}B^T \lambda _N ) = P_N Ax_{N-1}-P_N B R^{-1}B^T \lambda _N`$

$`\lambda _N + P_N B R^{-1}B^T \lambda _N = P_N Ax_{N-1}`$

$`\lambda _N = (I + P_N BR^{-1}B^T )^{-1} P_N A x_{N-1}`$

or :

$`\lambda _{t+1} = (I + P_{t+1} BR^{-1}B^T )^{-1} P_{t+1} A x_{t}`$

And using $`\lambda _{N-1} = A^T \lambda _N + Q`$ we get:

$`\lambda _{N-1} = A^T (I + P_N B R^{-1}B^T)^{-1}P_N A x_{N-1} + Qx_{N-1}`$

$`\lambda _{N-1} = [ A^T (I + P_N B R^{-1}B^T)^{-1}P_N A + Q ] x_{N-1} = P_{N-1}x_{N-1}`$

So we get $`\lambda _{N-1} = P_{N-1}x_{N-1}`$ where:

$`P_{N-1} = Q + A^T (I + P_N B R^{-1}B^T)^{-1}P_N A`$

or :

$`P_{t} = Q + A^T (I + P_{t+1} B R^{-1}B^T)^{-1}P_{t+1} A`$

is the **Dynamic Ricatti Equation**.

We can use these result to obtain a solution of $`u`$ in terms of the *Dynamic Ricatti Equation*:

$`u_t = -R^{-1} B^T \lambda _{t+1}`$

using $`\lambda _{t+1} = (I + P_{t+1} BR^{-1}B^T )^{-1} P_{t+1} A x_{t}`$ we get:

$`u_t = -R^{-1} B^T (I + P_{t+1}BR^{-1} B^T )^{-1} P_{t+1}Ax_t`$

using identity $`A(I+BA)^{-1} = (I+AB)^{-1}A`$ over $`B^T (I + P_{t+1}BR^{-1} B^T )^{-1}`$ we get:

$`u_t = -R^{-1} (I + B^T P_{t+1}BR^{-1} )^{-1}B^T P_{t+1}Ax_t`$

using identity $`B^{-1}A^{-1} = (AB)^{-1}`$ over $`R^{-1} (I + B^T P_{t+1}BR^{-1} )^{-1}`$ we get:

$`u_t = - \left( \left(  I + B^T P_{t+1}BR^{-1} \right) R \right) ^{-1}B^T P_{t+1}Ax_t`$

and finally simplifying:

$`u_t = - \left( R + B^T P_{t+1}B \right) ^{-1}B^T P_{t+1}Ax_t`$

So the optimal trajectory for $`u`$ at $`t=0, ..., N-1`$ can be computed backwards in time
assuming an end condition $`P_N`$ are recursing the equations:

$`P_{t} = Q + A^T (I + P_{t+1} B R^{-1}B^T)^{-1}P_{t+1} A`$

$`u_t = - \left( R + B^T P_{t+1}B \right) ^{-1}B^T P_{t+1}Ax_t`$

Or assume that the *Dynamic Ricatti Equation* converges ($`P_{t} = P_{t+1}`$) to a value $`P^*`$ which can be obtained by solving:

$`P^* = Q + A^T (I + P^* B R^{-1}B^T)^{-1}P^* A`$

And use $`P^*`$ for all time $`t`$:

$`u_{t} = - \left( R + B^T P^* B \right) ^{-1}B^T P^* Ax_{t}`$

---

# References

* <https://www.math.uwaterloo.ca/~hwolkowi/matrixcookbook.pdf>
